global find_word
extern string_equals
section .text


; list structure:
;   last element address - 8 bytes
;   key (pointer to string) - 8 bytes
;   value (pointer to value) - 8 bytes

; rdi - pointer to key to find
; rsi - pointer to last word in dictionary
find_word:
    .loop:
    test rsi, rsi
    jz .not_found


    add rsi, 8 ; moves pointer to key in list
    call string_equals ; compares rdi and rsi strings


    test rax, rax
    jnz .key_equals

    mov rsi, [rsi - 8]
    jmp .loop

    .not_found:
    xor rax, rax
    jmp .end

    .key_equals:
    mov rax, rsi

    .end:
    ret