AS = nasm
LD = ld
ASFLAGS = -g -f elf64
BUILD_DIR = build

all: main

main: main.o lib.o dict.o
	$(LD) $^ -o $@

%.o: %.asm
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -rf *.o main

main.o: words.inc colon.inc

.PHONY: clean