global exit
global string_length

global print_string
global print_error

global print_char
global print_newline

global print_int
global print_uint

global read_char
global read_word
global read_line

global parse_uint
global parse_int

global string_equals
global string_copy

section .data
ascii_symbols: db ` !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_\`abcdefghijklmnopqrstuvwxyz`, 0
null_char: db ' ', 0
test_message: db 'test', 0
buffer: times 256 db 0
new_line: db 10, 0

section .text


; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
     xor     rax, rax
    .loop:
        cmp byte [rdi + rax], 0
        je      .end
        inc     rax
        jmp     .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length

    mov rdx, rax ; string length
    mov rsi, rdi ; string pointer

    mov rax, 1 ; output syscall
    mov rdi, 1 ; stdout descriptor

    syscall

    ret

print_error:
    xor rax, rax
    call string_length

    mov rdx, rax ; string length
    mov rsi, rdi ; string pointer

    mov rax, 1 ; output syscall
    mov rdi, 2 ; stdout descriptor

    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax

    sub rdi, 0x20 ; subtract 32 (20 in hex) to change symbols range from [65; 122] to [0, 57] for easier offset calculation
    lea rsi, [ascii_symbols + rdi] ; put symbols to stdout

    mov rax, 1
    mov rdi, 1
    mov rdx, 1

    syscall

    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, new_line

    syscall

    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r8, 10 ; 10 is a base for division
    mov r9, rsp
    mov rax, rdi ; moves number to rax for division
    dec rsp
    mov byte[rsp], 0 ; clears first stack byte
    .division_loop:
    dec rsp
    xor rdx, rdx ; clears rdx for remainder
    div r8 ; performs division
    add rdx, '0' ; converts remainder to ascii
    mov byte[rsp], dl ; puts remainder byte to stack
    test rax, rax
    jnz .division_loop

    mov rdi, rsp
    call print_string

    mov rsp, r9 ; restores stack
    ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    mov rax, rdi
    test rax, rax
    jns .plus
    mov  rdi, '-'
    push rax
    call print_char
    pop rax
    neg rax
    mov rdi, rax
    .plus:
    call print_uint
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rbx

    xor rbx, rbx
    .cmp_loop:
    mov r8b, byte [rdi+rbx]
    mov r9b, byte [rsi+rbx]
    cmp r8b, r9b
    jne .char_not_equal
    test r8b, r8b
    jz .equals
    inc rbx
    jmp .cmp_loop

    .equals:
    mov rax, 1
    pop rbx
    ret

    .char_not_equal:
    mov rax, 0
    pop rbx
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    push rsi
    push rdx
    dec rsp

    xor rax, rax ; read syscall
    mov rdi, 0 ; stdin
    mov rdx, 1 ; bytes to read
    mov rsi, rsp
    syscall

    cmp rax, 0

    je .zero_return
    xor rax, rax
    mov al, byte [rsp]
    jmp .end

    .zero_return:
    mov rax, 0

    .end:
    inc rsp
    pop rdx
    pop rsi
    pop rdi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0x10.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера.
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx ; counter
    xor rbx, rbx

    .skip_spaces:

    call read_char

    cmp rax, 0x20
    je .skip_spaces
    cmp rax, 0x9
    je .skip_spaces
    cmp rax, 0xA
    je .skip_spaces

    .read_loop:

    cmp rbx, rsi
    jae .buffer_exceeded

    cmp rax, 0x20
    je .end_of_word
    cmp rax, 0x9
    je .end_of_word
    cmp rax, 0xA
    je .end_of_word
    test rax, rax
    je .end_of_word

    mov byte[rdi + rbx], al
    inc rbx

    call read_char
    jmp .read_loop

    .end_of_word:
    mov byte[rdi + rbx], 0x0
    mov rax, rdi
    mov rdx, rbx
    jmp .end

    .buffer_exceeded:
    mov rax, 0
    xor rdx, rdx
    jmp .end

    .end:
    pop rbx
    ret

; reads whole line from stdin
read_line:
    push rbx ; counter
    xor rbx, rbx

    .skip_spaces:

    call read_char

    cmp rax, 0x20
    je .skip_spaces
    cmp rax, 0x9
    je .skip_spaces
    cmp rax, 0xA
    je .skip_spaces

    .read_loop:

    cmp rbx, rsi
    jae .buffer_exceeded

    cmp rax, 0xA
    je .end_of_line
    test rax, rax
    je .end_of_line

    mov byte[rdi + rbx], al
    inc rbx

    call read_char
    jmp .read_loop

    .end_of_line:
    mov byte[rdi + rbx], 0x0
    mov rax, rdi
    mov rdx, rbx
    jmp .end

    .buffer_exceeded:
    mov rax, 0
    xor rdx, rdx
    jmp .end

    .end:
    pop rbx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi

    mov r8, 10

    xor rcx, rcx ; string element counter
    xor rdx, rdx

    .loop:
    mov sil, [rdi+rcx]
    cmp sil, 0x30 ; checks for ascii code in range [0x30, 0x39]

    jl .return
    cmp sil, 0x39
    jg .return

    inc rcx

    sub sil, 0x30 ; converts to number
    mul r8
    add rax, rsi
    jmp .loop

    .return:
    mov rdx, rcx
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], 0x2d ; checks for minus
    je parse_ng

    call parse_uint

    ret

    parse_ng:
    inc rdi ; moves number pointer

    call parse_uint

    cmp rdx, 0
    je .return

    neg rax
    inc rdx ; increase number length for 1 because of minus

    .return:
     ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

    call string_length

    cmp rax, rdx ; compares string length and buffer size

    jg .string_doesnt_fit

    .copy_loop:
    mov     cl, [rdi]
    inc     rdi
    mov     [rsi], cl
    inc     rsi
    cmp     cl, 0
    jne     .copy_loop
    jmp     .end


    .string_doesnt_fit:
    mov rax, 0

    .end:
    ret
