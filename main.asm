%include "words.inc"
%define BUFFER_SIZE 255

extern read_line
extern print_string
extern find_word
extern exit
extern print_error
extern print_newline
extern string_length

global _start

section .data
input_buffer: times BUFFER_SIZE db 0
error_message: db "Can't find element in a list", 10, 0

section .text
_start:
    mov rdi, input_buffer
    mov rsi, BUFFER_SIZE
    call read_line

    mov rsi, last_element
    call find_word

    test rax, rax
    jz .not_found

    mov rdi, rax

    call string_length

    add rdi, rax
    inc rdi

    call print_string
    call print_newline

    jmp .end

    .not_found:
    mov rdi, error_message
    call print_error

    .end:
    mov rdi, 1
    call exit


