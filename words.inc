%include "colon.inc"

section .data

colon "Metal", metal
db "Apple graphics API", 0

colon "OpenGL", opengl
db "Cross-platform graphics API", 0

colon "DirectX", directx
db "Windows only graphics API", 0

colon "Vulkan", vulkan
db "Vulkan cross-platform low-level graphics API", 0